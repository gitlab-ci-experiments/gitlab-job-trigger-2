#!/bin/bash
DEPLOY_TEST_JOB_ID=$(curl --header "PRIVATE-TOKEN: "$1"" 'https://gitlab.com/api/v4/projects/'$2'/pipelines/'$3'/jobs' | jq 'map(select(.stage == "'$4'"))[0].id')
echo "Deploy test job id: " $DEPLOY_TEST_JOB_ID
curl -X POST --header "PRIVATE-TOKEN: "$1"" -F token=1e0c4bad0e0a8a2a4635b2b1b4041d -F ref=master https://gitlab.com/api/v4/projects/3639803/jobs/$DEPLOY_TEST_JOB_ID/play
