#!/bin/bash
if [ "$1" != "" ] && [ "$1" != "BFF" ]; then
  ./skip_job.sh "HEALTH_CHECK_TYPE is BFF"
else
  echo "Running BFF healthcheck tests..."
fi
