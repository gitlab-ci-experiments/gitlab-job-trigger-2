#!/bin/bash
echo "HEALTH_CHECK_TYPE" $1
if [ "$1" != "" ] && [ "$1" != "UI" ]; then
  ./skip_job.sh "HEALTH_CHECK_TYPE is UI"
else
  echo "Running UI healthcheck tests..."
fi
